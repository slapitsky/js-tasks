(function() {
var map = function(arr, mapFunction) {
    var result = [];
    for (item of arr) {
        result.push(mapFunction(item));
    }

    return result
}
var mapFunctionDouble = function(value) {
    return value * 2 ;
}
var doubled = map([1,2,3,4,5,6,7,8,9], mapFunctionDouble);
alert(doubled);
})();