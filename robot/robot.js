(function() {
var directions = ['NORTH', 'EAST', 'SOUTH','WEST']
var Robot = class Robot {
    constructor(height, width) {
      this.x = 0;
      this.y = 0;
      this.direction = 'NORTH';
    }

    move() {
        if (this.direction==='NORTH') {
            this.y++;
        }
        else if (this.direction==='SOUTH') {
            this.y--;
        }
        else if (this.direction==='EAST') {
            this.x++;
        }
        else if (this.direction==='WEST') {
            this.x--;
        }
    }
    rotate(rotateDirection) {
        var index = directions.indexOf(this.direction)
        if ('RIGHT'===rotateDirection) {
            index++;
            if (index>3) {
                index =0
            }
        }
        else if ('LEFT'===rotateDirection) {
            index--;
            if (index<0) {
                index =3
            }
        }
        return directions[index]
    }
    left() {
        this.direction = this.rotate('LEFT');
    }
    right() {
        this.direction = this.rotate('RIGHT');
    }
    report() {
        return 'x=' +this.x +', y=' + this.y + ', direction=' + this.direction;
    }
}
var robot = new Robot();
robot.move();
robot.move();
robot.move();
robot.right();
robot.move();
robot.move();
alert(robot.report());
})();