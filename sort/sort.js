(function() {
//default one
var sort = function(source) {
    return source.sort();
}
//implemented one
var mergeSort = function(sortArray) {
    var sorted = [];
    for (item of sortArray) {
        var index = bTreeFindIndex(sorted, 0, sorted.length, item);
        sorted.splice(index, 0, item);
    }
    return sorted;
}
var bTreeFindIndex = function(sortArray, rangeStart, rangeEnd, value) {
    if (sortArray.length==0) {
        return 0;
    }
    while(rangeEnd>rangeStart) {

        if (rangeEnd - rangeStart == 1) {
             if (sortArray[rangeStart]<value) {
                 return rangeStart;
             }
             else {
                 return rangeEnd;
             }
        }
        var mid = rangeStart + Math.round((rangeEnd - rangeStart) / 2);
        if (value === sortArray[mid]) {
            return mid;
        }
        else if (sortArray[mid]<value) {
            rangeEnd = mid;
        }
        else {
            rangeStart = mid;
        }
    }
}

//var sorted = sort([1,9,2,8,3,7,4,6,5], compare);
var sorted = mergeSort([1,9,2,8,3,7,4,6,5,8,8,1]);
alert(sorted);
})();