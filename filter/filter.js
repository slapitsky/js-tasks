(function() {
var filter = function(arr, condition) {
    var result = [];
    for (item of arr) {
        if (condition(item)) {
            result.push(item);
        }
    }

    return result
}
var cEven = function(value) {
    return value % 2 === 0;
}
var evens = filter([1,2,3,4,5,6,7,8,9], cEven);
alert(evens);
})();