(function() {
var search = function(arr, condition) {
    for (item of arr) {
        if (condition(item)) {
            return item;
        }
    }

    return null
}
var cEven = function(value) {
    return value % 2 === 0;
}
var c5OrMore = function(value) {
    return value >= 5;
}
var firstEven = search([1,2,3,4,5,6,7,8,9], cEven);
alert(firstEven);
//var first5OrMore = search([1,2,3,4,  7,8,9], c5OrMore);
var first5OrMore = search([1,2,3,4], c5OrMore);
alert(first5OrMore);
})();